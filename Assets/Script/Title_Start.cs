﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Title_Start : MonoBehaviour,IPointerClickHandler {

    public Animator _ObjectsAni;
    public Image _thisImage;
	private bool _Started;

	//紀錄玩家玩到哪裡
	public static int _PlayerDay = 1;
	public static int _PlayerStage = 1;

	// Use this for initialization
	void Start () {
		_Started = false;
        _ObjectsAni = GameObject.Find("Objects").GetComponent<Animator>();
        _thisImage = gameObject.GetComponent<Image>();
	}

    void Update()
    {
		if (!_Started) {
			_thisImage.color = Color.Lerp (new Color32 (255, 255, 255, 255), new Color32 (255, 255, 255, 0), Mathf.PingPong (Time.time, 1));
		} else if (_Started) {
			_thisImage.color = Color.Lerp (_thisImage.color, new Color32 (255, 255, 255, 0), Time.deltaTime*1.5f);
		}
    }

    public void OnPointerClick(PointerEventData eventData)
    {
		if (!_Started) {
			_ObjectsAni.SetTrigger ("Started");
			_Started = !_Started;
		}
    }
}