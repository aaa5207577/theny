﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseLevel_Manager : MonoBehaviour {

    public GameObject[] _Calendars;
	public GameObject _CalendarTable;
	public GameObject _Camera;
	public GameObject _SceneManager;
	public GameObject _NG;
	public int _SoFar;
	bool _TArrive;
	Vector3 _P2;
	Vector3 _P0;

	void Start () {
		_TArrive = false;
		_Calendars = new GameObject[5];
		_Camera = GameObject.Find ("Main Camera");
		_SceneManager = GameObject.Find ("SceneManager");
		_CalendarTable = GameObject.Find ("ChooseLevel");
		_NG = GameObject.Find ("NewGame");
		_P0 = new Vector3 (10, 0,0);
		_P2 = new Vector3 (-10, 0,0);

		for (int i = 0; i < 5; i++) {
			_Calendars [i] = GameObject.Find ("Calendars").transform.GetChild (i).gameObject;
			_Calendars [i].SetActive (false);
		}
	}

	void Update(){
		
	}

	public void _Touched (string _ButtonName){
		Debug.Log ("It's " + _ButtonName);
	}

	public void _NewGame () {
        _SceneManager.SendMessage("_LoadScene", 2);
    }

    public void _ChooseLevel () {
        _Camera.transform.Translate(_P2);
		_CalendarTable.SetActive (false);
		_NG.SetActive (false);
		for (int i = 0 ; i <_SoFar ; i++) {
			_Calendars [i].SetActive (true);
		}
	}

	public void _Dx (int x){
		Debug.Log ("D" + x);
        _SceneManager.SendMessage("_LoadScene", 2);
    }

	public void _TableBack(){
        _Camera.transform.Translate(_P0);
        _CalendarTable.SetActive (true);
		_NG.SetActive (true);
		for (int i = 0 ; i <_SoFar ; i++) {
			_Calendars [i].SetActive (false);
		}
	}

}