﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Answers : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {

    GameObject _TheButton;
    Image _ImageOfThis;

    //int _OptionNum;

	void Start () {
        _TheButton = GameObject.Find("Button");
        _ImageOfThis = gameObject.GetComponent<Image>();
    }
    

    public void OnPointerEnter(PointerEventData eventData)
    {
        _TheButton.SendMessage("PlayerHere", gameObject.name);
        _ImageOfThis.color = new Color32(190,27,27,255);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _TheButton.SendMessage("PlayerOut");
        ResetColor();
    }
	
    void ResetColor()
    {
        _ImageOfThis.color = new Color32(140, 40, 40, 255);
    }
}