﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TheButton : MonoBehaviour , IPointerDownHandler, IPointerUpHandler
{

    public GameObject[] _Answers;
    public GameObject _AreaFence;
    public Text _TestText;
    public string _PlayerNow;

    void Start()
    {
        _Answers = new GameObject[3];
        _Answers[0] = GameObject.Find("AnswerA");
        _Answers[1] = GameObject.Find("AnswerB");
        _Answers[2] = GameObject.Find("AnswerC");
        _AreaFence = GameObject.Find("AreaFence");
        _AreaFence.SetActive(false);
    }

    public void PlayerHere(string _AnswerN)
    {
        switch (_AnswerN)
        {
            case "AnswerA":
                _PlayerNow = "A";
                break;

            case "AnswerB":
                _PlayerNow = "B";
                break;

            case "AnswerC":
                _PlayerNow = "C";
                break;
        }
    }

    public void PlayerOut()
    {
        _PlayerNow = null;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("D");
        _AreaFence.SetActive(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _TestText.text = _PlayerNow;

        for (int i=0;i<3;i++) {
            _Answers[i].SendMessage("ResetColor");
        }

        _AreaFence.SetActive(false);

    }
}