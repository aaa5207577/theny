﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_Objects : MonoBehaviour {

	public GameObject _SceneManager;

	void Start () {
		_SceneManager = GameObject.Find ("SceneManager");
	}

	public void _AniEnd(){
		_SceneManager.SendMessage ("_LoadScene", 1);
	}

}
