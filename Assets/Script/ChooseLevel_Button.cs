﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseLevel_Button : MonoBehaviour {

	public GameObject _Manager;

	void Start(){
		_Manager = GameObject.Find ("Manager");
	}

	public void _Touched(){
		_Manager.SendMessage ("_Touched",this.gameObject.name);
	}
}