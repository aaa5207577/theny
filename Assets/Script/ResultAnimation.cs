﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultAnimation : MonoBehaviour {

    public Animator _Ani;
    public GameObject _SceneManager;

	// Use this for initialization
	void Start () {

		//記錄玩到第五關
		Title_Start._PlayerStage = 5;
		PlayerPrefs.SetInt( "_PlayerStage",Title_Start._PlayerStage);

        _Ani = this.gameObject.GetComponent<Animator>();
        _SceneManager = GameObject.Find("SceneManager");
    }
	
    public void AniStart()
    {
        _Ani.SetTrigger("Start");
    }

	//清除玩家資料（測試用）
	public void _ClearPlayerRecord()
	{
		PlayerPrefs.SetInt( "_PlayerDay",0);
		PlayerPrefs.SetInt( "_PlayerStage",0);
	}

	//撕掉日曆結束一天
    public void _AniEnd()
    {
		int temp = PlayerPrefs.GetInt ("_PlayerDay") + 1;
		//天數加一天，記錄起來
		PlayerPrefs.SetInt("_PlayerDay",temp);

		//回到第二關（這裡是開頭動畫）
        _SceneManager.SendMessage("_LoadScene",2);
    }
	
}
