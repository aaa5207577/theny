﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene_Manager : MonoBehaviour {

	public void _LoadScene(int i){
		Debug.Log("Day：" + PlayerPrefs.GetInt("_PlayerDay") + "，Stage：" + i);
		SceneManager.LoadScene (i);
	}
}
