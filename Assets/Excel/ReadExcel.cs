﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadExcel : MonoBehaviour {

	[SerializeField]
	Entity_Story _Story;

	// Use this for initialization
	void Start () {

		//記錄玩到第四關
		Title_Start._PlayerStage = 4;
		PlayerPrefs.SetInt( "_PlayerStage",Title_Start._PlayerStage);

		switch(PlayerPrefs.GetInt("_PlayerDay"))
		{
			case 1:
			//讀Excel
			for(int i=0 ; i<_Story.sheets.Count;i++)
			{
				//分頁（Sheet）
				if (_Story.sheets[i].name.Equals ("Day1")) 
				{
					for (int j=0; j<_Story.sheets[0].list.Count; j++) 
					{
						//欄位
						Debug.Log(_Story.sheets[0].list[j].Name + "：" + _Story.sheets[0].list[j].Story);
					}
				}
			}
			break;

			case 2:
			//讀Excel
			for(int i=0 ; i<_Story.sheets.Count;i++)
			{
				//分頁（Sheet）
				if (_Story.sheets[i].name.Equals ("Day2")) 
				{
					for (int j=0; j<_Story.sheets[1].list.Count; j++) 
					{
						//欄位
						Debug.Log(_Story.sheets[1].list[j].Name + "：" + _Story.sheets[1].list[j].Story);
					}
				}
			}
			break;

			case 3:
			//讀Excel
			for(int i=0 ; i<_Story.sheets.Count;i++)
			{
				//分頁（Sheet）
				if (_Story.sheets[2].name.Equals ("Day3")) 
				{
					for (int j=0; j<_Story.sheets[i].list.Count; j++) 
					{
						//欄位
						Debug.Log(_Story.sheets[2].list[j].Name + "：" + _Story.sheets[2].list[j].Story);
					}
				}
			}
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
